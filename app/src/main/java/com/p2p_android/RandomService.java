package com.p2p_android;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.p2p_android.App.CHANNEL_ID;

public class RandomService extends Service {

    /**
     * toggles foreground notification of service
     */
    Boolean foreground = true;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * initiates network server
     */
    @Override
    public void onCreate() {
        try {

            ExecutorService executorService = Executors.newSingleThreadExecutor();
            executorService.execute(new NetworkServiceThread(5000));

            if( foreground ) writeNotify("running");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("devel","passed to onCreate");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public boolean stopService(Intent name) {
        return super.stopService(name);
    }

    /**
     * creates notification for service
     */
    public void writeNotify(String input) {
        Intent notiIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notiIntent, 0);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Random Service")
                .setContentText(input)
                .setSmallIcon(R.drawable.ic_android_black_24dp)
                .setContentIntent(pendingIntent)
                .build();

        startForeground(1234,notification);
    }

    /**
     * Handle single connection
     */
    class NetworkServiceThread implements Runnable {
        private final ServerSocket serverSocket;

        NetworkServiceThread(int port)
                throws IOException {
            serverSocket = new ServerSocket(port);
        }

        @Override
        public void run() {

            try {
                while (true) {
                    // accept tcp connection
                    Socket socket = serverSocket.accept();

                    Log.i("Server", String.format("Request %s %s",
                            socket.getInetAddress(), socket.getPort()));

                    // send new random number as String to client
                    try {
                        PrintWriter printWriter =
                                new PrintWriter(new OutputStreamWriter( socket.getOutputStream()));

                        printWriter.println(String.format("%s",new Random().nextInt(100)));
                        printWriter.flush();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

//==================================================================================================

    /**
     * Handle multi connections, executes incoming requests in parallel
     */
    class NetworkServiceThreadMultiConn implements Runnable {
        private final ServerSocket serverSocket;
        private final ExecutorService pool;

        NetworkServiceThreadMultiConn(int port)
                throws IOException {
            serverSocket = new ServerSocket(port);
            pool = Executors.newFixedThreadPool(10);
        }

        @Override
        public void run() {

            try {
                while (true) {
                    pool.execute(new ConnectionHandler( serverSocket.accept() ) );
                }
            } catch (IOException ex) {
                ex.printStackTrace();
                pool.shutdown();
            }
        }

        class ConnectionHandler implements Runnable {

            private final Socket socket;

            ConnectionHandler(Socket socket) {
                this.socket = socket;
            }

            @Override
            public void run() {

                Log.i("Server",
                        String.format("Request %s %s", socket.getInetAddress(), socket.getPort()));

                // send new random number as String to client
                try {
                    PrintWriter printWriter =
                            new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));

                    printWriter.println(String.format("%s", new Random().nextInt(100)));
                    printWriter.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}