package com.p2p_android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class MainActivity extends Activity {
    // sum of generated numbers
    private int sum = 0;
    private TextView sumView;

    // list of generated numbers
    private ListView numberListView;
    private final ArrayList<Integer> numberList = new ArrayList<>();
    private ArrayAdapter adapter;

    // Service Address
    private final String address = "127.0.0.1";
    private final int port = 5000;

    /**
     * Instance creation
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);

        sumView = findViewById(R.id.sumOfNumbers);
        numberListView = findViewById(R.id.oldNumbers);

        adapter = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1, numberList);
        numberListView.setAdapter(adapter);

        //DEBUG: start service on activity start
        //Intent serviceIntent = new Intent(this, RandomService.class);
        //startService(serviceIntent);
    }

    public void startService(View v) {
        Intent serviceIntent = new Intent(this, RandomService.class);
        startService(serviceIntent);
    }

    public void stopService(View v) {
        Intent serviceIntent = new Intent(this, RandomService.class);
        stopService(serviceIntent);
    }

    /** Called when "Neue Zufallszahl" button is clicked.
     *  Contacts background service to generate a new random number and updates Views. */
    public void onButtonClick(View v) throws ExecutionException, InterruptedException {

        ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<Integer> call = executor.submit(new RandomNumberTask());

        int currentNumber = call.get();;
        sum += currentNumber;

        sumView.setText(String.format("Summe: %s",sum));
        numberList.add(currentNumber);

        adapter.notifyDataSetChanged();
    }


    /**
     * Callable for interacting with the server.
     * Calling it returns the random number the server sends as response or 0 if an error is thrown.
     */
    private class RandomNumberTask implements Callable<Integer> {

        @Override
        public Integer call() {
            try {
                // connect to server
                Socket client = new Socket(address, port);

                // parse received String to Integer
                int response = Integer.parseInt(receiveString(client));

                //immediately close connection afterwards
                client.close();

                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return 0;
            }
        }
    }

    /**
     * receive a string from a socket connection
     */
    private String receiveString(Socket socket) throws IOException {
        BufferedReader bufferedReader =
                new BufferedReader(
                        new InputStreamReader(
                                socket.getInputStream()));
        return bufferedReader.readLine();
    }
}
