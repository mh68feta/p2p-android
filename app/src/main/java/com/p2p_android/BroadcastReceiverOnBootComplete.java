package com.p2p_android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

public class BroadcastReceiverOnBootComplete extends BroadcastReceiver {

    /**
     * Triggers on boot complete,
     * starts service as foreground service if build version is Oreo or higher.
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        if ( Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction()) ) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(new Intent(context, RandomService.class));
            } else {
                context.startService(new Intent(context, RandomService.class));
            }
        }
    }
}